use itertools::Itertools;

struct GameData {
    t: u64,
    dmax: u64,
}

fn ceil_roundup(val: f64) -> f64 {
    let ceil = val.ceil();
    if ceil == val {
        val + 1.0
    } else {
        ceil
    }
}
fn floor_rounddown(val: f64) -> f64 {
    let floor = val.floor();
    if floor == val {
        val - 1.0
    } else {
        floor
    }
}

impl GameData {
    pub fn count_solutions(&self) -> u64 {
        let t_f = self.t as f64;
        let d_f = self.dmax as f64;

        let x_min = t_f / 2.0 - ((t_f * t_f) / 4.0 - d_f).sqrt();
        let x_max = t_f / 2.0 + ((t_f * t_f) / 4.0 - d_f).sqrt();

        let x_min = ceil_roundup(x_min) as u64;
        let x_max = floor_rounddown(x_max) as u64;

        println!(
            "Game t={}, d={} has solutions x_min={}, x_max={}",
            self.t, self.dmax, x_min, x_max
        );

        // add 1, as range is inclusive
        x_max - x_min + 1
    }
}

fn parse_games<'a>(input: &'a str) -> impl Iterator<Item = GameData> + 'a {
    let lines = input.lines().collect_vec();
    let times = lines[0].split_once(":").unwrap().1;
    let dists = lines[1].split_once(":").unwrap().1;

    let times = times.split_whitespace().map(|t| t.parse().unwrap());
    let dists = dists.split_whitespace().map(|d| d.parse().unwrap());

    times.zip(dists).map(|(t, dmax)| GameData { t, dmax })
}

fn part1(input: &str) {
    let games = parse_games(input);
    let product: u64 = games.map(|g| g.count_solutions()).product();

    println!("Part 1: {}", product);
}

fn part2(input: &str) {
    let lines = input.lines().collect_vec();
    let times = lines[0].split_once(":").unwrap().1;
    let dists = lines[1].split_once(":").unwrap().1;

    let time: u64 = times.replace(" ", "").parse().unwrap();
    let dist: u64 = dists.replace(" ", "").parse().unwrap();

    let game = GameData{t: time, dmax: dist};
    println!("Part 2: {}", game.count_solutions());
}

fn main() {
    // let input = include_str!("../../inputs/day6/small");
    let input = include_str!("../../inputs/day6/full");
    part1(input);
    part2(input);
}
