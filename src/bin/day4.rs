struct InputLine {
    winning_nrs: Vec<u32>,
    chosen_nrs: Vec<u32>,
}

impl InputLine {
    pub fn match_cnt(&self) -> usize {
        self.chosen_nrs
            .iter()
            .filter(|c| self.winning_nrs.contains(c))
            .count()
    }
    pub fn score(&self) -> u32 {
        let chosen_cnt = self.match_cnt();
        if chosen_cnt == 0 {
            0
        } else {
            2u32.pow((chosen_cnt - 1) as u32)
        }
    }
}

fn parse_numbers<'a>(input: &'a str) -> impl Iterator<Item = InputLine> + 'a {
    input
        .lines()
        .map(|l| l.split_once(':').unwrap().1)
        .map(|l| l.split_once('|').unwrap())
        .map(|(win, ch)| InputLine {
            winning_nrs: win.split_whitespace().map(|s| s.parse().unwrap()).collect(),
            chosen_nrs: ch.split_whitespace().map(|s| s.parse().unwrap()).collect(),
        })
}

fn part1(input: &str) {
    let win_sum: u32 = parse_numbers(input).map(|l| l.score()).sum();
    println!("Part 1: {}", win_sum)
}

fn part2(input: &str) {
    let cards: Vec<_> = parse_numbers(input).collect();
    let mut copies: Vec<_> = cards.iter().map(|_| 1).collect();

    for (i, c) in cards.iter().enumerate() {
        for j in 1..=c.match_cnt() {
            copies[i + j] += copies[i];
        }
    }
    let total_cards: u32 = copies.iter().sum();
    println!("Part 2: {}", total_cards)
}

fn main() {
    // let input = include_str!("../../inputs/day4/small");
    let input = include_str!("../../inputs/day4/full");
    part1(input);
    part2(input)
}
