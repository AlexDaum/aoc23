use err_derive::Error;
use std::{cmp::max, num::ParseIntError, str::FromStr};

struct CubeSet {
    red: u32,
    green: u32,
    blue: u32,
}

struct Game {
    id: u32,
    sets: Vec<CubeSet>,
}

impl CubeSet {
    pub fn possible(&self, max: &CubeSet) -> bool {
        self.red <= max.red && self.green <= max.green && self.blue <= max.blue
    }

    pub fn partwise_max(&self, oth: &CubeSet) -> CubeSet {
        CubeSet {
            red: max(self.red, oth.red),
            green: max(self.green, oth.green),
            blue: max(self.blue, oth.blue),
        }
    }

    pub fn power(&self) -> u32 {
        self.red * self.green * self.blue
    }
}

#[derive(Debug, Error)]
enum ParseErr {
    #[error(display = "Invalid Format {}", _0)]
    InvalidFormat(String),
    #[error(display = "Error parsing Integer: {}", _0)]
    ParseIntError(#[error(from)] ParseIntError),
}

impl FromStr for CubeSet {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // comma separated list of (int color) pairs
        let mut set = CubeSet {
            red: 0,
            green: 0,
            blue: 0,
        };
        for part in s.split(",") {
            match part.trim().split_once(" ") {
                Some((count, color)) => {
                    let count: u32 = count.parse()?;
                    match color {
                        "red" => set.red += count,
                        "green" => set.green += count,
                        "blue" => set.blue += count,
                        _ => return Err(ParseErr::InvalidFormat(color.to_string())),
                    }
                }
                None => return Err(ParseErr::InvalidFormat(part.to_string())),
            }
        }
        Ok(set)
    }
}

impl FromStr for Game {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim().split_once(":") {
            Some((game, sets)) => {
                let sets: Result<Vec<CubeSet>, _> =
                    sets.split(";").map(|s| s.parse::<CubeSet>()).collect();
                let sets = sets?;
                let id: u32 = game.trim()["Game".len()..].trim().parse()?;
                Ok(Game { id, sets })
            }
            None => Err(ParseErr::InvalidFormat(s.to_string())),
        }
    }
}

const MAXIMUM: CubeSet = CubeSet {
    red: 12,
    green: 13,
    blue: 14,
};

fn part1(input: &str) {
    let games: Result<Vec<_>, _> = input.lines().map(|g| g.parse::<Game>()).collect();

    let binding = games.expect("Format Error");
    let possible_games = binding
        .iter()
        .filter(|g| g.sets.iter().all(|s| s.possible(&MAXIMUM)));
    let sum: u32 = possible_games.map(|g| g.id).sum();
    println!("Part1: Sum of possible IDs: {}", sum);
}

fn part2(input: &str) {
    let games: Result<Vec<_>, _> = input.lines().map(|g| g.parse::<Game>()).collect();
    let binding = games.expect("Format Error");

    let powers = binding
        .iter()
        .map(|g| {
            g.sets.iter().fold(
                CubeSet {
                    red: 0,
                    blue: 0,
                    green: 0,
                },
                |s1, s2| s1.partwise_max(&s2),
            )
        })
        .map(|s| s.power());
    let sum: u32 = powers.sum();
    println!("Part1: Sum of possible IDs: {}", sum);
}

fn main() {
    // let input = include_str!("../../inputs/day2/small1");
    let input = include_str!("../../inputs/day2/big");
    part1(input);
    part2(input);
}
