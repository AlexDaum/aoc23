

// fn print_pipe(l: u32) -> u32 {
//     println!("{}", l);
//     l
// }

fn per_line(l: &str) -> u32 {
    let s = l.to_string();
    let s = s.replace("one", "one1one");
    let s = s.replace("two", "two2two");
    let s = s.replace("three", "three3three");
    let s = s.replace("four", "four4four");
    let s = s.replace("five", "five5five");
    let s = s.replace("six", "six6six");
    let s = s.replace("seven", "seven7seven");
    let s = s.replace("eight", "eight8eight");
    let s = s.replace("nine", "nine9nine");

    let mut iter = s.chars().filter_map(|c| c.to_digit(10));
    let first = iter.next().unwrap();
    let last = iter.last().unwrap_or(first);

    10 * first + last
}

fn main() {
    // let input = include_str!("../../inputs/day1_small2.txt");
    // let input = include_str!("../../inputs/day1_big.txt");
    let input = include_str!("../../inputs/day1_big.txt");
    let sum: u32 = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| per_line(l))
        // .map(print_pipe)
        .sum();
    println!("The sum is {}", sum);
}
