use std::array::TryFromSliceError;

use aoc23::diophantine_eq::CycleProblem;
use itertools::Itertools;
use rustc_hash::FxHashMap;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Node {
    name: [char; 3]
}

impl TryFrom<&[char]> for Node {
    type Error = TryFromSliceError;

    fn try_from(value: &[char]) -> Result<Self, Self::Error> {
        Ok(
            Node {
                name: value.try_into()?
            }
          )
    }
}

impl TryFrom<Vec<char>> for Node{
    type Error = Vec<char>;

    fn try_from(value: Vec<char>) -> Result<Self, Self::Error> {
        Ok(
            Node {
                name: value.try_into()?
            }
          )
    }
}

struct Connections {
    l: Node,
    r: Node,
}

// impl std::hash::Hash for Node {
//     fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
//         let mut reduce = self.name[0] as u32 - 'A' as u32;
//         reduce = 26 * reduce + self.name[0] as u32 - 'A' as u32;
//         reduce = 26 * reduce + self.name[0] as u32 - 'A' as u32;
//
//         state.write_u16(reduce as u16);
//         state.finish();
//     }
// }

struct Graph {
    adjacency_map: FxHashMap<Node, Connections>,
}

impl Graph {
    pub fn left(&self, n: &Node) -> Option<Node> {
        self.adjacency_map.get(n).map(|c| c.l.clone())
    }
    pub fn right(&self, n: &Node) -> Option<Node> {
        self.adjacency_map.get(n).map(|c| c.r.clone())
    }

    pub fn distance<F>(&self, n: &Node, rlline: &str, is_target: F) -> usize
    where
        F: Fn(&Node) -> bool,
    {
        let mut node = n.clone();
        let mut steps = 0;
        for rl in rlline.chars().cycle() {
            steps += 1;
            let next = if rl == 'R' {
                self.right(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no right", node))
            } else {
                self.left(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no left", node))
            };
            if node == next {
                println!("Self-reference detected!");
                break;
            }
            node = next;
            if is_target(&node) {
                break;
            }
        }
        steps
    }

    pub fn find_cycle(&self, n_start: &Node, rlline: &str, skip: usize) -> usize {
        // find the length of a cycle in the path starting from this node
        let n = self.walk(n_start, rlline, skip);
        let mut node = n.clone();
        for (step, rl) in rlline.chars().cycle().skip(skip % rlline.len()).enumerate() {
            let next = if rl == 'R' {
                self.right(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no right", node))
            } else {
                self.left(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no left", node))
            };
            node = next;
            if node == n {
                return step + 1;
            }
        }
        panic!("WTF")
    }

    pub fn walk(&self, n: &Node, rlline: &str, steps: usize) -> Node {
        let mut node = n.clone();
        for rl in rlline.chars().cycle().take(steps) {
            let next = if rl == 'R' {
                self.right(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no right", node))
            } else {
                self.left(&node)
                    .unwrap_or_else(|| panic!("Node {:?} has no left", node))
            };
            if node == next {
                println!("Self-reference detected!");
                break;
            }
            node = next;
        }
        node
    }
}

fn parse_graph<'a>(input: impl Iterator<Item = &'a str>) -> Graph {
    let mut g = Graph {
        adjacency_map: FxHashMap::default(),
    };

    for l in input {
        let (label, connections) = l.split_once("=").unwrap();
        let (left, right) = connections.split_once(",").unwrap();
        // remove access chars and ugly hacky parsing into the char array
        let left: Node = left.trim()[1..]
            .trim()
            .chars()
            .collect_vec()
            .try_into()
            .unwrap();
        let right: Node = right.trim()[..3].chars().collect_vec().try_into().unwrap();
        let node: Node = label.trim().chars().collect_vec().try_into().unwrap();
        g.adjacency_map
            .insert(node, Connections { l: left, r: right });
    }
    g
}

fn part1(input: &str) {
    let mut lines = input.lines();
    let rlline = lines.nth(0).unwrap();
    let graph = parse_graph(lines.skip(1));

    let node = Node {name: ['A', 'A', 'A']};
    let target = ['Z', 'Z', 'Z'];
    let steps = graph.distance(&node, rlline, |n| n.name == target);
    println!("Part 1: {}", steps);
}

fn part2(input: &str) {
    let mut lines = input.lines();
    let rlline = lines.nth(0).unwrap();
    let graph = parse_graph(lines.skip(1));

    let nodes = graph
        .adjacency_map
        .keys()
        .filter(|n| n.name[2] == 'A')
        .map(|n| n.clone())
        .collect_vec();

    let steps_per_node = nodes
        .iter()
        .map(|n| graph.distance(n, rlline, |n| n.name[2] == 'Z'))
        .collect_vec();

    let cycle_per_node = nodes
        .iter().enumerate()
        .map(|(i, n)| graph.find_cycle(n, rlline, steps_per_node[i]))
        .collect_vec();

    // sanity checks
    // for i in 0..nodes.len() {
    //     println!("From Node: {:?}", nodes[i]);
    //     let target = graph.walk(&nodes[i], rlline, steps_per_node[i]);
    //     println!("Target Node {:?} in {} steps", target, steps_per_node[i]);
    //     let cycle_target = graph.walk(&nodes[i], rlline, steps_per_node[i] + cycle_per_node[i]);
    //     println!("Cycled back to Node {:?} after {} steps", cycle_target, cycle_per_node[i]);
    //     let cycle_target2 = graph.walk(&nodes[i], rlline, steps_per_node[i] + 2*cycle_per_node[i]);
    //     println!("Cycled back to Node {:?} after {} steps", cycle_target2, 2*cycle_per_node[i]);
    // }

    let cycles = steps_per_node.iter().zip(&cycle_per_node).map(|(s, c)| ((*s).try_into().unwrap(), (*c).try_into().unwrap())).collect_vec();
    let cp = CycleProblem::from_cycles(cycles);
    println!("Part 2: {}", cp.solve().expect("Could not solve Part 2"));
}

fn main() {
    // let input = include_str!("../../inputs/day8/small");
    // let input = include_str!("../../inputs/day8/small2");
    // let input = include_str!("../../inputs/day8/small3");
    let input = include_str!("../../inputs/day8/full");
    part1(input);
    part2(input);

}
