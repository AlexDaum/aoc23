use regex::Regex;

#[derive(Debug)]
struct Number {
    line: usize,
    begin: usize,
    end: usize,
    value: u32,
}

struct Symbol {
    line: usize,
    pos: usize,
    value: char,
}

impl Number {
    pub fn is_adjacent(&self, sym: &Symbol) -> bool {
        if self.line.abs_diff(sym.line) > 1 {
            false
        } else {
            self.begin <= sym.pos.saturating_add(1) && self.end >= sym.pos.saturating_sub(1)
        }
    }
}

fn parse_numbers_in_line<'a>(
    line_nr: usize,
    line: &'a str,
    num_re: &'a Regex,
) -> impl Iterator<Item = Number> + 'a {
    num_re.find_iter(line).map(move |m| Number {
        line: line_nr,
        begin: m.start(),
        end: m.end() - 1,
        value: m.as_str().parse().expect("WTF number Regex wrong?"),
    })
}

fn parse_numbers<'a>(input: &'a str, num_re: &'a Regex) -> impl Iterator<Item = Number> + 'a {
    input
        .lines()
        .enumerate()
        .map(|(i, l)| parse_numbers_in_line(i, l, num_re))
        .flatten()
}

fn parse_symbols_in_line<'a>(line_nr: usize, line: &'a str) -> impl Iterator<Item = Symbol> + 'a {
    line.chars()
        .enumerate()
        .filter(|(_i, c)| !c.is_digit(10) && *c != '.')
        .map(move |(pos, value)| Symbol {
            line: line_nr,
            pos,
            value,
        })
}

fn parse_symbols<'a>(input: &'a str) -> impl Iterator<Item = Symbol> + 'a {
    input
        .lines()
        .enumerate()
        .map(|(i, l)| parse_symbols_in_line(i, l))
        .flatten()
}

fn part1(input: &str) {
    let num_re = Regex::new(r"[0-9]+").unwrap();
    let numbers: Vec<_> = parse_numbers(input, &num_re).collect();
    let syms: Vec<_> = parse_symbols(input).collect();

    let sum: u32 = numbers
        .iter()
        .filter(|n| syms.iter().any(|s| n.is_adjacent(s)))
        .map(|n| n.value)
        .sum();

    println!("Part 1: {}", sum)
}

fn part2(input: &str) {
    let num_re = Regex::new(r"[0-9]+").unwrap();
    let numbers: Vec<_> = parse_numbers(input, &num_re).collect();
    let gears = parse_symbols(input)
        .filter(|s| s.value == '*')
        .filter(|s| numbers.iter().filter(|n| n.is_adjacent(&s)).count() == 2);
    let gear_sum: u32 = gears
        .map(|s| {
            numbers
                .iter()
                .filter(|n| n.is_adjacent(&s))
                .map(|n| n.value)
                .product::<u32>()
        })
        .sum();
    println!("Part 2: {}", gear_sum);
}

fn main() {
    // let input = include_str!("../../inputs/day3/small");
    let input = include_str!("../../inputs/day3/full");
    part1(input);
    part2(input)
}
