use itertools::Itertools;

#[derive(Debug, PartialEq, Eq, Ord)]
struct Card {
    label: char,
}

impl Card {
    pub fn value(&self) -> u32 {
        match self.label {
            '*' => 0,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9,
            'T' => 10,
            'J' => 11,
            'Q' => 12,
            'K' => 13,
            'A' => 14,
            _ => panic!("invalid Card"),
        }
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.value().partial_cmp(&other.value())
    }
}

#[derive(Debug, PartialEq, Eq, Ord)]
struct Hand {
    cards: Vec<Card>,
    bid: u32,
    strength: u32,
}

impl Hand {
    pub fn new(cards: &str, bid: u32) -> Self {
        let cards = cards.chars().map(|label| Card { label }).collect_vec();

        let strength = Self::strength(&cards);
        Self { cards, bid, strength }
    }

    fn strength(cards: &Vec<Card>) -> u32 {
        let groups = cards
            .iter()
            .filter(|c| c.label != '*') // filter jokers
            .sorted()
            .group_by(|c| *c)
            .into_iter()
            .map(|(key, g)| (key, g.count()))
            .sorted_by(|k1, k2| Ord::cmp(&k2.1, &k1.1))
            .collect_vec();

        let jokers = cards.iter().filter(|c| c.label == '*').count();

        // println!("Calculating strength... Groups: {:?}", groups);

        // special case: 5 jokers
        if groups.len() == 0 {
            assert!(jokers == 5);
            return 6;
        }
        match groups[0].1 + jokers {
            5 => 6, // five of a kind
            4 => 5, // four of a kind
            3 => {
                if groups[1].1 == 2 {
                    4
                } else {
                    3
                }
            }
            2 => {
                if groups[1].1 == 2 {
                    2
                } else {
                    1
                }
            }
            _ => 0,
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let my_str = self.strength;
        let oth_str = other.strength;
        if my_str == oth_str {
            self.cards.partial_cmp(&other.cards)
        } else {
            my_str.partial_cmp(&oth_str)
        }
    }
}

fn part1(input: &str) {
    let hands = input
        .lines()
        .map(|s| s.split_once(" ").unwrap())
        .map(|(hand, bid)| {
            Hand::new(
                hand.trim(),
                bid.trim()
                    .parse()
                    .expect(&format!("Error parsing bid {:?}", bid)),
            )
        })
        .sorted();

    // for ele in &hands {
    // println!("Strength: {}, Hand {:?}", ele.strength(), ele);
    // }

    let total_winnings: usize = hands
        .enumerate()
        .map(|(num, hand)| (num + 1) * hand.bid as usize)
        .sum();
    println!("Part 1: {}", total_winnings);
}

fn part2(input: &str) {
    let hands = input
        .lines()
        .map(|s| s.split_once(" ").unwrap())
        .map(|(hand, bid)| {
            Hand::new(
                &hand.trim().replace("J", "*"),
                bid.trim()
                    .parse()
                    .expect(&format!("Error parsing bid {:?}", bid)),
            )
        })
        .sorted();
    let total_winnings: usize = hands
        .enumerate()
        .map(|(num, hand)| (num + 1) * hand.bid as usize)
        .sum();
    println!("Part 2: {}", total_winnings);
}

fn main() {
    // let input = include_str!("../../inputs/day7/small");
    let input = include_str!("../../inputs/day7/full");
    part1(input);
    part2(input);
}
