use bit_vec::BitVec;
use itertools::Itertools;

use log::debug;

#[derive(Default, Clone, Copy, Debug)]
struct AOCNode {
    idx: usize,
    width: usize,
    label: char,
}

struct AOCGrid {
    width: usize,
    nodes: Vec<AOCNode>,
    start_idx: usize,
}

impl AOCNode {
    pub fn new(idx: usize, width: usize, label: char) -> Self {
        Self { idx, width, label }
    }
    pub fn north(&self) -> Option<usize> {
        self.idx.checked_sub(self.width)
    }
    pub fn south(&self) -> Option<usize> {
        Some(self.idx + self.width)
    }
    pub fn east(&self) -> Option<usize> {
        if self.idx % self.width == self.width - 1 {
            None
        } else {
            Some(self.idx + 1)
        }
    }
    pub fn west(&self) -> Option<usize> {
        self.idx.checked_sub(1)
    }
    pub fn successors<'a>(&'a self) -> impl Iterator<Item = usize> + 'a {
        NodeSuccessorIterator {
            origin: self,
            state: 0,
        }
    }
    pub fn is_corner(&self) -> bool {
        self.label == '7' || self.label == 'J' || self.label == 'F' || self.label == 'L'
    }
}
struct NodeSuccessorIterator<'a> {
    origin: &'a AOCNode,
    state: u32,
}

impl<'a> NodeSuccessorIterator<'a> {
    fn find_possible_next(&mut self) -> Option<Option<usize>> {
        let mut r = match (self.state, self.origin.label) {
            (_, '.') => None,
            (0, '|') => Some(self.origin.north()),
            (1, '|') => Some(self.origin.south()),
            (0, '-') => Some(self.origin.east()),
            (1, '-') => Some(self.origin.west()),
            (0, 'L') => Some(self.origin.east()),
            (1, 'L') => Some(self.origin.north()),
            (0, 'J') => Some(self.origin.west()),
            (1, 'J') => Some(self.origin.north()),
            (0, '7') => Some(self.origin.west()),
            (1, '7') => Some(self.origin.south()),
            (0, 'F') => Some(self.origin.east()),
            (1, 'F') => Some(self.origin.south()),
            (0, 'S') => Some(self.origin.north()),
            (1, 'S') => Some(self.origin.east()),
            (2, 'S') => Some(self.origin.south()),
            (3, 'S') => Some(self.origin.west()),
            _ => None,
        };
        while let Some(None) = r {
            self.state += 1;
            r = self.find_possible_next();
        }
        self.state += 1;
        r
    }
}

impl<'a> Iterator for NodeSuccessorIterator<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.find_possible_next().map(|o| o.unwrap())
    }
}

fn parse_input(input: &str) -> AOCGrid {
    // handling of the Start position...
    // There should only be one possible position where the s can connect to two
    let width = input.lines().nth(0).unwrap().len();

    let nodes = input
        .chars()
        .filter(|c| !c.is_whitespace())
        .enumerate()
        .map(|(i, c)| AOCNode::new(i, width, c))
        .collect_vec();

    let start_idx = nodes
        .iter()
        .find_position(|n| n.label == 'S')
        .expect("No starting position")
        .0;

    AOCGrid {
        width,
        nodes,
        start_idx,
    }
}
fn find_loop_len_iterative(grid: &mut AOCGrid, open: &mut BitVec) -> Option<Vec<usize>> {
    let mut the_loop = vec![];
    let mut stack = vec![];
    stack.push((grid.start_idx, 0));

    let mut last = grid.start_idx;
    while !stack.is_empty() {
        let (current, step) = stack.pop().unwrap();
        debug!(
            "Visiting {}, {}: {}",
            current / grid.width,
            current % grid.width,
            grid.nodes[current].label
        );

        // check if legal
        if current != grid.start_idx && !grid.nodes[current].successors().contains(&last) {
            debug!("Illegal move");
            continue;
        }
        the_loop.resize(step, 0);
        the_loop.push(current); // assume part of the loop
        open.set(current, false);

        for suc in grid.nodes[current]
            .successors()
            .filter(|s| *s < grid.nodes.len())
            .filter(|s| open[*s] || *s == grid.start_idx)
            .filter(|s| *s != last)
            .collect_vec()
        {
            if suc == grid.start_idx {
                debug!(
                    "Found Start again at {}, {}",
                    suc / grid.width,
                    suc % grid.width
                );
                return Some(the_loop);
            }
            stack.push((suc, step + 1));
        }
        last = current;
    }
    None
}

fn count_inside_in_line(grid: &AOCGrid, loop_set: &BitVec, y: usize) -> usize {
    let mut area = 0;
    let mut state = false;
    let mut last_corner = None;
    for x in 0..grid.width {
        let idx = x + grid.width * y;
        let curr_node = &grid.nodes[idx];
        if loop_set[idx] {
            if curr_node.label == '|' {
                state = !state;
            } else if curr_node.is_corner() {
                match last_corner {
                    None => last_corner = Some(curr_node.label),
                    Some(c) => {
                        last_corner = None;
                        if match (c, curr_node.label) {
                            ('F', '7') => false,
                            ('F', 'J') => true,
                            ('L', '7') => true,
                            ('L', 'J') => false,
                            e => panic!("Should not happen, I think {}{}", e.0, e.1),
                        } {
                            state = !state;
                        }
                    }
                }
            }
            // print!("{}", curr_node.label);
        } else if state {
            area += 1;
            // print!("I");
        } else {
            // print!("O");
        }
    }
    area
}

fn part2(input: &str) {
    let mut grid = parse_input(input);
    // start at the start position and consider
    let mut open = BitVec::from_elem(grid.nodes.len(), true);
    // let start_idx = grid.start_idx;
    let the_loop =
        find_loop_len_iterative(&mut grid, &mut open).expect("Invalid Input: No Loop found");

    // correctly replace S with its actual value
    let next = the_loop[1];
    let last = *the_loop.last().unwrap();
    let diff = next.abs_diff(last);
    // cases
    // - => 2
    // | => 2*width
    // L => width + 1, neighbor bigger than start
    // F => width - 1, start_idx = min
    // J => width - 1, start_idx = max
    // 7 => width + 1, neighbor smaller than start
    grid.nodes[grid.start_idx].label = if diff == 2 {
        '-'
    } else if diff == 2 * grid.width {
        '|'
    } else if diff == grid.width + 1 {
        // find neighbor
        if grid.start_idx == next + 1 || grid.start_idx == last + 1 {
            '7'
        } else {
            'L'
        }
    } else if diff == grid.width - 1 {
        if grid.start_idx > next {
            'J'
        } else {
            'F'
        }
    } else {
        panic!("Impossible start!")
    };

    // The Algorithm:
    // Scan linewise
    // Each line start marked as OUTSIDE
    // If we encounter a vertical tile, then switch between OUTSIDE to INSIDE
    // vertical tiles are: part of the loop, | -> toggle.
    // Corners have to be combined to know if they toggle. e.g
    // * L7 toggles, it is └┐
    // * LJ does not toggle, it is └┘
    let mut loop_set = BitVec::from_elem(grid.nodes.len(), false);
    for n in the_loop {
        loop_set.set(n, true);
    }

    let mut area = 0;
    for y in 0..(grid.nodes.len() / grid.width) {
        area += count_inside_in_line(&grid, &loop_set, y);
        // println!("");
    }

    println!("Part 2: {}", area);
}

fn part1(input: &str) {
    let mut grid = parse_input(input);
    // start at the start position and consider
    let mut open = BitVec::from_elem(grid.nodes.len(), true);
    // let start_idx = grid.start_idx;
    let the_loop = find_loop_len_iterative(&mut grid, &mut open);
    println!("Part 1: {:?}", the_loop.map(|l| l.len() / 2))
}
fn main() {
    env_logger::init();
    // let input = include_str!("../../inputs/day10/part2_small3");
    let input = include_str!("../../inputs/day10/full");
    // part1(input);
    part2(input);
}
