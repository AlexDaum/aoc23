use std::{ops::Range};

use aoc23::range_map::{RangeRemapper, RemapperChain};
use itertools::Itertools;

fn from_inupt<'a, T>(input: impl IntoIterator<Item = T> + 'a) -> RemapperChain<u64>
where
    T: IntoIterator<Item = &'a str>,
{
    let mut chain = RemapperChain::new();

    for g in input {
        let mut g = g.into_iter();
        let _ = g.next().expect("Invalid Input");
        let mut map = RangeRemapper::new();
        for l in g.map(|s| {
            s.split_whitespace()
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<_>>()
        }) {
            let dest_start = l[0];
            let src_start = l[1];
            let len = l[2];
            map.insert(src_start..src_start + len, dest_start..dest_start + len);
        }
        chain.add_remapper(map);
    }

    chain
}

fn parse_seeds(seed_line: &str) -> Vec<u64> {
    seed_line
        .split_once(':')
        .unwrap()
        .1
        .trim()
        .split_whitespace()
        .map(|s| {
            s.parse()
                .expect(&format!("Invalid seed line {}", seed_line))
        })
        .collect()
}

fn parse_seeds2(seed_line: &str) -> Vec<Range<u64>> {
    let seed_ranges: Vec<(u64, u64)> = seed_line
        .split_once(':')
        .unwrap()
        .1
        .trim()
        .split_whitespace()
        .map(|s| {
            s.parse()
                .expect(&format!("Invalid seed line {}", seed_line))
        })
        .tuples()
        .collect();

    seed_ranges.into_iter().map(|(b, l)| b..b + l).collect()
}

fn part1(input: &str) {
    let grouped = input.lines().group_by(|l| l.is_empty());

    let mut groups = grouped.into_iter().filter(|(k, _g)| !k).map(|(_k, g)| g);

    let mut seeds = groups.next().expect("No Input?");
    let maps = from_inupt(groups);

    let seeds = parse_seeds(seeds.nth(0).unwrap());

    let min_location = seeds.iter().map(|s| maps.remap_one(*s)).min();
    //
    // println!("The seeds are: {:?}", seeds);
    // println!("Seed to soil map: {:?}", maps.seed_to_soil);

    println!("Part1: {}", min_location.unwrap());
}

fn part2(input: &str) {
    let grouped = input.lines().group_by(|l| l.is_empty());

    let mut groups = grouped.into_iter().filter(|(k, _g)| !k).map(|(_k, g)| g);

    let mut seeds = groups.next().expect("No Input?");
    let maps = from_inupt(groups);

    let seeds = parse_seeds2(seeds.nth(0).unwrap());

    let remapped_ranges = seeds.into_iter().flat_map(|s| maps.remap(s));

    let min_location = remapped_ranges.map(|r| r.start).min();
    //
    // println!("The seeds are: {:?}", seeds);
    // println!("Seed to soil map: {:?}", maps.seed_to_soil);

    println!("Part2: {}", min_location.unwrap());
}

fn main() {
    // let input = include_str!("../../inputs/day5/small");
    let input = include_str!("../../inputs/day5/full");
    part1(input);
    part2(input);
}
