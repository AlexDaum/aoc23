use itertools::Itertools;

#[derive(Debug)]
struct DeconstructedSequence {
    differences: Vec<Vec<i32>>,
}

impl DeconstructedSequence {
    fn create_difference(sequence: &Vec<i32>) -> Vec<i32> {
        sequence
            .iter()
            .tuple_windows()
            .map(|(a, b)| b - a)
            .collect_vec()
    }
    pub fn from_sequence(sequence: Vec<i32>) -> Self {
        let mut diffs = vec![];
        diffs.push(sequence);
        while !diffs.last().unwrap().iter().all(|v| *v == 0)  {
            diffs.push(Self::create_difference(diffs.last().unwrap()));
        }        
        Self { differences: diffs }
    }

    pub fn evaluate_next(&self) -> i32 {
        self.differences.iter().rev().map(|e| e.last().unwrap()).sum()
    }

    fn evaluate_prev(&self) -> i32 {
        self.differences.iter().rev().map(|e| e[0]).reduce(|acc, e| e - acc).unwrap()
    }
}

fn parse_input(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|l| {
            l.split_whitespace()
                .map(|s| s.parse().unwrap())
                .collect_vec()
        })
        .collect_vec()
}

fn part1(input: &str) {
    let input = parse_input(input);
    let mut sum = 0;
    for a in input {
        let dec = DeconstructedSequence::from_sequence(a);
        // println!("p({}) = {}", a.len(), dec.evaluate_next());
        sum += dec.evaluate_next();
    }
    println!("Part 1: {}", sum)
}

fn part2(input: &str) {
    let input = parse_input(input);
    let mut sum = 0;
    for a in input {
        let dec = DeconstructedSequence::from_sequence(a);
        // println!("prev for sequence {:?} = {} ", dec.differences[0], dec.evaluate_prev());
        sum += dec.evaluate_prev();
    }
    println!("Part 2: {}", sum)
}

fn main() {
    // let input = include_str!("../../inputs/day9/small");
    let input = include_str!("../../inputs/day9/full");
    part1(input);
    part2(input);
}
