use std::fmt::Debug;

use err_derive::Error;
use itertools::Itertools;
use num::traits::Euclid;

/// A function that implements the extended Euclidean algorithm. The function accepts two numbers:
/// - The first number is the number whose inverse is to be found (a)
/// - The modulo in which the operation will be carried out (b)
/// The function returns a tuple with three values:
/// - The GCD of a and b
/// - x (As explained in the extended Euclidean Algorithm above)
/// - y (As explained in the extended Euclidean Algorithm above)
fn extended_euclidean_algorithm<T: num::Signed + Copy>(a: T, b: T) -> (T, T, T) {
    // This is the base case: when b = 0,
    //     gcd(a, 0) = a
    // Hence the Euclidean equation becomes:
    //     a(1) + b(0) = a
    if b == num::zero() {
        return (a, num::one(), num::zero());
    }

    // Recursively call the extended Euclidean Algorithm
    let (gcd, x1, y1) = extended_euclidean_algorithm(b, a % b);

    // Compute x and y by working backwards the Euclidean Algorithm
    let x = y1;
    let y = x1 - (a / b) * y1;

    // Return the tuple
    return (gcd, x, y);
}

/// Models a linear diophantine equation in two variables x and y
/// in the form ax + by = c
#[derive(Debug, Clone, PartialEq)]
pub struct LDE<T: num::Signed> {
    a: T,
    b: T,
    c: T,
}

#[derive(Clone, Debug)]
pub struct LDESolution<T: num::Signed> {
    pub x0: T,
    pub y0: T,
    pub dx: T,
    pub dy: T,
}

#[derive(Error, Debug, Clone, PartialEq)]
#[error(display = "could not solve the LDE")]
pub enum LDEError {
    #[error(display = "LDE has no solution")]
    NoSolution,
}

impl<T: num::Signed + Copy + Ord + Euclid> LDE<T> {
    pub fn new(a: T, b: T, c: T) -> Self {
        Self { a, b, c }
    }

    ///
    /// Solve a Linear Diophantine Equation
    ///
    ///
    pub fn solve(&self) -> Result<LDESolution<T>, LDEError> {
        let (gcd, xx, yy) = extended_euclidean_algorithm(self.a, self.b);
        if self.c.rem(gcd) != num::zero() {
            Err(LDEError::NoSolution)
        } else {
            let x0 = xx * self.c / gcd;
            let y0 = yy * self.c / gcd;
            let dx = self.b / gcd;
            let dy = -self.a / gcd;
            Ok(LDESolution { x0, y0, dx, dy })
        }
    }

    fn get_positive(x0: T, dx: T) -> T {
        x0 - x0.div_euclid(&dx) * dx
    }

    pub fn solve_min_positive(&self) -> Result<LDESolution<T>, LDEError> {
        let mut initial = self.solve()?;
        
        initial.dx = initial.dx.abs();
        initial.dy = initial.dy.abs();

        initial.x0 = Self::get_positive(initial.x0, initial.dx);
        initial.y0 = Self::get_positive(initial.y0, initial.dy);

        Ok(initial)
    }
}

/// A special form of LDE System in the form of
///
/// a1x + b1y = c1
/// a2y + b2z = c2
/// a3z + b3w = c3
/// ...
#[derive(Debug, Clone)]
pub struct ChainLDESystem<T: num::Signed> {
    ldes: Vec<LDE<T>>,
}

impl<T: num::Signed + Copy + Clone + Debug + Ord + Euclid> ChainLDESystem<T> {
    pub fn from_vec(ldes: Vec<LDE<T>>) -> Self {
        Self { ldes }
    }

    /// Reduce the System of n LDEs to one of n-1 LDEs for its solutions
    fn reduce(&self) -> Result<ChainLDESystem<T>, LDEError> {
        // first solve all ldes
        let sols = self.ldes.iter().map(LDE::solve);
        let pairs = sols.tuple_windows();
        let new_ldes = pairs.map(|sols| match sols {
            (Ok(sol1), Ok(sol2)) => Ok(LDE::new(sol1.dy, -sol2.dx, sol2.x0 - sol1.y0)),
            (Err(e), _) => Err(e),
            (_, Err(e)) => Err(e),
        });
        let new_ldes: Result<Vec<_>, _> = new_ldes.collect();
        let new_ldes = new_ldes?;
        Ok(ChainLDESystem::from_vec(new_ldes))
    }

    /// Solves a chained system of LDEs.
    ///
    /// The solution is provided as a vector.
    /// When the System has solutions
    /// x = x0 + k*dx
    /// y = y0 + k*dy
    /// ....
    /// The result is
    /// [(x0, dx), (y0, dy), ...]
    pub fn solve(&self) -> Result<Vec<(T, T)>, LDEError> {
        if self.ldes.len() == 1 {
            // end case
            let solved = self.ldes[0].solve()?;
            let vec = vec![(solved.x0, solved.dx.abs()), (solved.y0, solved.dy.abs())];
            Ok(vec)
        } else {
            let reduced_solutions = self.reduce()?.solve()?;
            let mut res = vec![];
            for (i, sol) in reduced_solutions.iter().enumerate() {
                let top_sol = self.ldes[i].solve()?;
                if i == 0 {
                    res.push((top_sol.x0 + top_sol.dx * sol.0, (top_sol.dx * sol.1).abs()))
                }
                res.push((top_sol.y0 + top_sol.dy * sol.0, (top_sol.dy * sol.1).abs()))
            }
            Ok(res)
        }
    }

    pub fn solve_positive(&self) -> Result<Vec<(T, T)>, LDEError> {
        let solutions = self.solve()?;
        // find minimum increment
        let pos_solutions = solutions.into_iter().map(|(base, inc)| (base - inc * base.min(num::zero()).div_euclid(&inc), inc));
        Ok(pos_solutions.collect())
    }
}

pub struct CycleProblem {
    /// A list of (initial, length) pairs
    cycles: Vec<(i128, i128)>,
}

impl CycleProblem {
    pub fn from_cycles(cycles: impl IntoIterator<Item = (i128, i128)>) -> Self {
        Self {
            cycles: cycles.into_iter().collect(),
        }
    }

    fn solve_rec(cycles: &[(i128, i128)]) -> Result<(i128, i128), LDEError> {
        if cycles.len() == 0 {
            panic!("solve called with no cycles");
        }
        if cycles.len() == 1 {
            return Ok(cycles[0]);
        }


        let first = cycles[0];
        let rest = &cycles[1..];
        let grouped_cycle = Self::solve_rec(rest)?;

        let lde_sol = LDE::new(first.1, -grouped_cycle.1, grouped_cycle.0 - first.0).solve_min_positive()?;

        Ok((first.0 + first.1 * lde_sol.x0, first.1 * lde_sol.dx))
    }

    /// Solve the cycle problem
    ///
    /// Finds the smallest number of steps after which all cycles are at their starting position
    pub fn solve(&self) -> Result<i128, LDEError> {
        // step 1: translate to diophantine equations
        // let tuples = self.cycles.iter().tuple_windows();
        // let ldes = tuples.map(|(a, b)| LDE::new(a.1, -b.1, b.0 - a.0)).collect();
        // let system = ChainLDESystem::from_vec(ldes);
        // let solutions = system.solve_positive()?;
        // Ok(self.cycles[0].0 + self.cycles[0].1 * solutions[0].0)

        // Alternative solution: recursion!
        let total_cycle = Self::solve_rec(&self.cycles)?;
        Ok(total_cycle.0)
    }
}

#[cfg(test)]
mod test {
    use crate::diophantine_eq::CycleProblem;

    use super::{LDE, ChainLDESystem, LDEError};

    #[test]
    fn test_lde() {
        // 2x + y = 5
        let lde = LDE::new(2, 1, 5);
        let solutions = lde.solve();
        assert!(solutions.is_ok());
        let solutions = solutions.unwrap();
        assert_eq!(solutions.x0, 0);
        assert_eq!(solutions.y0, 5);
        assert_eq!(solutions.dx, 1);
        assert_eq!(solutions.dy, -2);

        let lde = LDE::new(3, -4, -3);
        let solutions = lde.solve();
        assert!(solutions.is_ok());
        let solutions = solutions.unwrap();
        assert_eq!(solutions.x0, 3);
        assert_eq!(solutions.y0, 3);
        assert_eq!(solutions.dx, 4);
        assert_eq!(solutions.dy, 3);
    }

    #[test]
    fn chained_lde() -> Result<(), LDEError>{
        let ldes = ChainLDESystem::from_vec(vec![LDE::new(3, -4, -3), LDE::new(4, -5, 1)]);
        let solutions = ldes.solve_positive()?;
        assert_eq!(solutions[0], (11, 20));
        assert_eq!(solutions[1], (9, 15));
        assert_eq!(solutions[2], (7, 12));


        Ok(())
    }

    #[test]
    fn test_cycle_solver() -> Result<(), LDEError>{
        let cycles = vec![(3, 3), (0, 4)];
        assert_eq!(12, CycleProblem::from_cycles(cycles).solve()?);

        let cycles = vec![(3, 3), (0, 4), (1, 5)];
        assert_eq!(36, CycleProblem::from_cycles(cycles).solve()?);

        Ok(())
    }
}
