use std::ops::{Add, Range, Sub};

use itertools::Itertools;
use rangemap::RangeMap;

pub struct RangeRemapper<T>
where
    T: Ord,
{
    map: RangeMap<T, Range<T>>,
}

impl<T> RangeRemapper<T>
where
    T: Ord + Clone + Eq + Sub<Output = T> + Add<Output = T> + Copy + std::fmt::Debug,
{
    pub fn new() -> Self {
        Self {
            map: RangeMap::new(),
        }
    }

    pub fn insert(&mut self, from: Range<T>, to: Range<T>) {
        self.map.insert(from, to);
    }

    pub fn get_including(&self, t: T) -> Option<&Range<T>> {
        self.map.get(&t)
    }

    // Checks if the given range r overlaps any range inside the range map
    pub fn overlaps(&self, r: Range<T>) -> bool {
        self.map.overlaps(&r.into())
    }

    fn clip_overlap(overarching: &Range<T>, key: &Range<T>, value: &Range<T>) -> Range<T> {
        let mut output = value.clone();
        if key.start < overarching.start {
            // println!(
            //     "Clipping Range {:?}..{:?}-->{:?}..{:?}, because it exceeds {:?}..{:?}",
            //     key.start, key.end, value.start, value.end, overarching.start, overarching.end
            // );
            let diff = overarching.start - key.start;
            output.start = output.start + diff;
        }
        if key.end > overarching.end {
            let diff = key.end - overarching.end;
            output.end = output.end - diff;
        }
        output
    }

    pub fn remap_one(&self, t: T) -> T {
        let kv = self.map.get_key_value(&t);
        kv.map(|(k, v)| v.start + t - k.start).unwrap_or(t)
    }

    pub fn remap<'a>(&'a self, r: &'a Range<T>) -> impl Iterator<Item = Range<T>> + 'a {
        let overlaps = self.map.overlapping(r);
        let gaps = self.map.gaps(r);
        // gaps can be used as-is, overlaps have to be clipped...
        let overlaps = overlaps.map(|(k, v)| Self::clip_overlap(r, k, v));
        overlaps.chain(gaps)
    }
}

pub struct RemapperChain<T>
where
    T: Ord,
{
    maps: Vec<RangeRemapper<T>>,
}

impl<T> RemapperChain<T>
where
    T: Ord + Clone + Eq + Sub<Output = T> + Add<Output = T> + Copy + std::fmt::Debug,
{
    pub fn new() -> Self {
        Self { maps: vec![] }
    }

    pub fn add_remapper(&mut self, map: RangeRemapper<T>) {
        self.maps.push(map)
    }

    pub fn remap_one(&self, v: T) -> T {
        let mut val = v;
        for m in &self.maps {
            val = m.remap_one(val);
        }
        val
    }

    pub fn remap<'a>(&'a self, r: Range<T>) -> impl Iterator<Item = Range<T>> + 'a {
        // println!("Chained remap input: {:?}..{:?}", r.start, r.end);
        let mut v = vec![r];
        for m in &self.maps {
            v = v.iter().flat_map(|i| m.remap(i)).collect_vec();
            // v.iter().for_each(|r| println!("Chained remap content: {:?}..{:?}", r.start, r.end));
            // println!("");
        }
        v.into_iter()
    }
}

#[cfg(test)]
mod test {
    use itertools::Itertools;
    use rangemap::RangeMap;

    use crate::range_map::RemapperChain;

    use super::RangeRemapper;

    #[test]
    fn test_range_map_overlapping() {
        // These are checks if we are using the API of RangeMap correctly
        let mut map = RangeMap::new();
        map.insert(10..20, 50..60);
        map.insert(30..40, 10..20);

        let overlaps50 = map.overlapping(&(0..50)).collect_vec();
        assert_eq!(overlaps50[0].0.start, 10);
        assert_eq!(overlaps50[0].0.end, 20);
        assert_eq!(overlaps50[0].1.start, 50);
        assert_eq!(overlaps50[0].1.end, 60);
        assert_eq!(overlaps50[1].0.start, 30);
        assert_eq!(overlaps50[1].0.end, 40);
        assert_eq!(overlaps50[1].1.start, 10);
        assert_eq!(overlaps50[1].1.end, 20);

        let overlapspartial = map.overlapping(&(15..35)).collect_vec();
        assert_eq!(overlapspartial[0].0.start, 10);
        assert_eq!(overlapspartial[0].0.end, 20);
        assert_eq!(overlapspartial[0].1.start, 50);
        assert_eq!(overlapspartial[0].1.end, 60);
        assert_eq!(overlapspartial[1].0.start, 30);
        assert_eq!(overlapspartial[1].0.end, 40);
        assert_eq!(overlapspartial[1].1.start, 10);
        assert_eq!(overlapspartial[1].1.end, 20);
    }

    #[test]
    fn test_range_remapper() {
        let mut map = RangeRemapper::new();
        map.insert(10..20, 50..60);
        map.insert(30..40, 10..20);

        let remapped = map.remap(&(10..40)).collect_vec();
        assert_eq!(remapped.len(), 3);
        assert_eq!(remapped[0].start, 50);
        assert_eq!(remapped[0].end, 60);
        assert_eq!(remapped[1].start, 10);
        assert_eq!(remapped[1].end, 20);
        // gaps go last
        assert_eq!(remapped[2].start, 20);
        assert_eq!(remapped[2].end, 30);
    }

    #[test]
    fn test_remapper_chain() {
        let mut map1 = RangeRemapper::new();
        map1.insert(10..20, 50..60);
        map1.insert(30..40, 100..110);

        let mut map2 = RangeRemapper::new();
        map2.insert(100..110, 10..20);

        let mut chain = RemapperChain::new();
        chain.add_remapper(map1);
        chain.add_remapper(map2);

        let remapped = chain.remap(10..40).collect_vec();
        for r in &remapped {
            println!("Final remapped range {}..{}", r.start, r.end);
        };
        assert_eq!(remapped.len(), 3);
        assert_eq!(remapped[0].start, 50);
        assert_eq!(remapped[0].end, 60);
        assert_eq!(remapped[1].start, 10);
        assert_eq!(remapped[1].end, 20);
        // gaps go last
        assert_eq!(remapped[2].start, 20);
        assert_eq!(remapped[2].end, 30);
    } 

    #[test]
    fn test_remapper_chain_not_contained() {
        let mut map1 = RangeRemapper::new();
        map1.insert(10..20, 50..60);
        map1.insert(30..40, 100..110);

        let mut map2 = RangeRemapper::new();
        map2.insert(35..55, 0..20);

        let mut chain = RemapperChain::new();
        chain.add_remapper(map1);
        chain.add_remapper(map2);

        let remapped = chain.remap(10..40).collect_vec();
        assert_eq!(remapped.len(), 4);
        assert_eq!(remapped[0].start, 15);
        assert_eq!(remapped[0].end, 20);
        assert_eq!(remapped[1].start, 55);
        assert_eq!(remapped[1].end, 60);
        assert_eq!(remapped[2].start, 100);
        assert_eq!(remapped[2].end, 110);
        // gaps go last
        assert_eq!(remapped[3].start, 20);
        assert_eq!(remapped[3].end, 30);
    }
}
